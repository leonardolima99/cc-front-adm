import gql from 'graphql-tag';

export const CREATE_ARTICLE = gql`
  mutation createArticle($title: String!, $articleAuthors: [String]!, $body: String!, $topic: ID!) {
    createArticle(input: {
      title: $title
      articleAuthors: $articleAuthors
      body: $body
      topic: $topic
      references: ""
    }) {
      article {
        title
        articleAuthors
        publicationDate
      }
    }
  }
`
export const CREATE_TOPIC = gql`
  mutation createTopic($name: String!, $description: String!, $scope: String!, $portal: ID!) {
    createTopic(input: {
      name: $name
      description: $description
      scope: $scope
      portal: $portal
    }) {
      topic {
        id
        name
        id
        scope
        portal {
          name
        }
      }
    }
  }
`

export const CREATE_PORTAL = gql`
  mutation createPortal($name: String!) {
    createPortal(input: {
      name: $name
    }) {
      portal {
        id
        name
        foundingDatetime
      }
    }
  }
`

export const LOGIN_MUTATION = gql`
  mutation login($username: String!, $password: String!) {
      logIn(username: $username, password: $password) {
          token
      }
  }
`;

export const REGISTER_MUTATION = gql`
  mutation register($username: String!, $email: String!, $password: String!, $birthdate: Date!) {
    signUp(input:{
        username: $username,
        password: $password,
        email: $email,
        birthDate: $birthdate,
        clientMutationId: $username
      }) {
          clientMutationId
      }
  }
`;

export const LOGOUT = gql`
  mutation logout {
    logOut(input:{}) {
          response
      }
  }
`;