import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import Home from './Components/Home';
import Login from './Components/Login/Login';
import Register from './Components/Register/Register';
import constants from './constants';

const authToken = localStorage.getItem(constants.AUTH_TOKEN) ? true : false;

function PrivateRoute({ component: Component, authenticated, ...rest }) {
    return (
        <Route
            {...rest}
            render={(props) => authenticated === true
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
        />
    )
}

function Routes(props) {
    return (
        <Switch>
            <PrivateRoute exact path="/" authenticated={authToken} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Redirect from="*" to="/login" />
        </Switch>
    )
}

export default Routes;