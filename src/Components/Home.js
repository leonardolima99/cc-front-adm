import React from 'react';
import CreatePortal from './CreatePortal'
import CreateTopic from './CreateTopic'
import CreateArticle from './CreateArticle'

function Home(props) {
  return (
    <React.Fragment>
      <div className="container mt-5">
        <CreatePortal />
      </div>
      <div className="container mt-5">
        <CreateTopic />
      </div>
      <div className="container mt-5">
        <CreateArticle />
      </div>
      <div className="mb-5"></div>
      
    </React.Fragment>
  )
}

export default Home;