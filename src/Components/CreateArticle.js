import React from 'react'
import { Mutation } from 'react-apollo'
import { CREATE_ARTICLE } from '../mutations'

export default class CreateArticle extends React.Component {

  state = {
    title: '',
    articleAuthors: [],
    body: '',
    topic: '',
    data: {}
  }

  render() {
    const { title, articleAuthors, body, topic } = this.state
    console.log(articleAuthors)
    return (
      <React.Fragment>
        <h2>Criar Artigo</h2>
        <form className="mt-3">
          <input className="form-control mb-3" type="text" placeholder="Título" 
            required onChange={e => this.setState({ title: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Autor do Artigo" 
            required onChange={e => this.setState({ articleAuthors: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Corpo" 
            required onChange={e => this.setState({ body: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Tópico" 
            required onChange={e => this.setState({ topic: e.target.value })} />
          
          <Mutation
            mutation={CREATE_ARTICLE}
            variables={{ title, articleAuthors, body, topic }}
            onCompleted={data => console.log(data)}
            onError={error => console.error(error)}
          >
            {mutation => (
              <button 
                className="btn btn-primary"
                type="button"
                onClick={mutation}
                disabled={!title || !articleAuthors || !body || !topic }
                >
                Criar
              </button>
            )}
          </Mutation>
        </form>
      </React.Fragment>
    )
  }
}