import React from 'react'
import { Mutation } from 'react-apollo'
import { CREATE_TOPIC } from '../mutations'

export default class CreateTopic extends React.Component {

  state = {
    name: '',
    description: '',
    scope: '',
    portal: '',
    data: {}
  }

  render() {
    const { name, description, scope, portal } = this.state
    return (
      <React.Fragment>
        <h2>Criar Tópico</h2>
        <form className="mt-3">
          <input className="form-control mb-3" type="text" placeholder="Nome" 
            required onChange={e => this.setState({ name: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Descrição" 
            required onChange={e => this.setState({ description: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Escopo" 
            required onChange={e => this.setState({ scope: e.target.value })} />
          <input className="form-control mb-3" type="text" placeholder="Portal" 
            required onChange={e => this.setState({ portal: e.target.value })} />
          
          <Mutation
            mutation={CREATE_TOPIC}
            variables={{ name, description, scope, portal }}
            onCompleted={data => this.setState({ data })}
            onError={error => console.error(error)}
          >
            {mutation => (
              <button 
                className="btn btn-primary"
                type="button"
                onClick={mutation}
                disabled={!name || !description || !scope || !portal }
                >
                Criar
              </button>
            )}
          </Mutation>
        </form>
      </React.Fragment>
    )
  }
}