import React from 'react'
import { Mutation } from 'react-apollo'
import { CREATE_PORTAL } from '../mutations'

export default class CreatePortal extends React.Component {

  state = {
    name: '',
    data: {}
  }

  render() {
    const { name } = this.state
    return (
      <React.Fragment>
        <h2>Criar Portal</h2>
        <form className="mt-3">
          <input 
            id="name" 
            className="form-control mb-3" 
            type="text" 
            placeholder="Nome" 
            required
            onChange={e => this.setState({ name: e.target.value })}
          />
          <Mutation
            mutation={CREATE_PORTAL}
            variables={{ name }}
            onCompleted={data => this.setState({ data })}
            onError={error => console.error(error)}
          >
            {mutation => (
              <button 
                className="btn btn-primary"
                type="button"
                onClick={mutation}
                disabled={!name}
                >
                Criar
              </button>
            )}
          </Mutation>
        </form>
      </React.Fragment>
    )
  }
}