import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import Routes from './Routes'
import Header from './Components/Header'
import './App.css';

function App() {
  return (
    <div className="wrapper">
      <BrowserRouter>
        <Header />
        <Routes />
      </BrowserRouter>
    </div>
  );
}

export default App;
